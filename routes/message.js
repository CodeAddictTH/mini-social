var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource');
});
//------------------------------------------------------------------------------
router.get("/feed/:lastid?", function(req, res, next){
    
    var lastid = 0;
    
    if(req.params.lastid!=undefined) lastid = req.params.lastid;
    
    res.send("last id : "+lastid);
});
//------------------------------------------------------------------------------
module.exports = router;
